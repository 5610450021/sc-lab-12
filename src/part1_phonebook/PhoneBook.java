package part1_phonebook;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class PhoneBook {
	private List<Contact> contact ;
	
	public PhoneBook(){
		contact = new ArrayList<Contact>();
		String filename = "phonebook.txt";
		FileReader fileReader = null;
		try {
			 fileReader = new FileReader(filename);
			 BufferedReader buffer = new BufferedReader(fileReader);

			 String line;
			 for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				 String[] data = line.split(", ");
				 String name = data[0].trim();
				 String number = data[1].trim();
				 Contact c = new Contact(name, number);
				 contact.add(c);
				 System.out.println(c.toString());
			 }
	 	 }
	 	catch (FileNotFoundException e){
			 System.err.println("Cannot read file "+filename);
	 	 }	 	
	 	catch (IOException e){
			 System.err.println("Error reading from file");
	 	 }
		finally {
			 try {
				 if (fileReader != null)
					 fileReader.close();
			 } catch (IOException e) {
				 System.err.println("Error closing files");
			 }
	 	 }
	}
	public void add(Contact c){
		contact.add(c);
	}
	
	public void delete(String name){
		contact.remove(name);
	}
	
	
	
}
