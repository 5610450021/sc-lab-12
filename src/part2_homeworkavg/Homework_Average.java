package part2_homeworkavg;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Homework_Average {
	private List<Student> stu ;
	private List<Student> stu2;
	public Homework_Average(){
		stu = new ArrayList<Student>();
		stu2 = new ArrayList<Student>();
		String filename = "homework.txt";
		FileReader fileReader = null;
		FileWriter fileWriter = null;
		try {
			 fileReader = new FileReader(filename);
			 BufferedReader buffer = new BufferedReader(fileReader);
			 FileReader fileReader2 = new FileReader("exam.txt");
			 BufferedReader buffer2 = new BufferedReader(fileReader2);
			 String line;
			 fileWriter = new FileWriter("average.txt");
			 PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));
			 out.println("--------- Homework Scores ---------");
			 out.println("Name Average");
			 out.println("==== =======");
			 for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				 String[] data = line.split(", ");
				 String name = data[0].trim();
				 double num1 = Double.parseDouble(data[1].trim());
				 double num2 = Double.parseDouble(data[2].trim());
				 double num3 = Double.parseDouble(data[3].trim());
				 double num4 = Double.parseDouble(data[4].trim());
				 double num5 = Double.parseDouble(data[5].trim());
				 double average = num1 + num2 + num3 + num4 + num5;
				 Student s = new Student(name);
				 
				 out.println(s.Avg(average).toString());
				 stu.add(s);
				 
				 
			 }
			 out.println("--------- Exam Scores ---------");
			 out.println("Name Average");
			 out.println("==== =======");
			 String line2;
			 for (line2 = buffer2.readLine(); line2 != null; line2 = buffer2.readLine()) {
				 String[] data2 = line2.split(", ");
				 String name2 = data2[0].trim();
				 double number1 = Double.parseDouble(data2[1].trim());
				 double number2 = Double.parseDouble(data2[2].trim());
				 double averageexam = number1 + number2 ;
				 Student s = new Student(name2);
				 out.println(s.AvgExam(averageexam).toString());
				 stu2.add(s);
			 }
			 out.flush();
			 out.close();
	 	 }
	 	catch (FileNotFoundException e){
			 System.err.println("Cannot read file "+filename);
	 	 }	 	
	 	catch (IOException e){
			 System.err.println("Error reading from file");
	 	 }
		finally {
			 try {
				 if (fileReader != null)
					 fileReader.close();
			 } catch (IOException e) {
				 System.err.println("Error closing files");
			 }
	 	 }
	}
}


//
//	public static void main(String[] args) {
//		FileWriter fileWriter = null;
//		try {
//			   FileReader fileReader = new FileReader("homework.txt");
//			   BufferedReader buffer = new BufferedReader(fileReader);
//			   FileReader fileReader2 = new FileReader("exam.txt");
//			   BufferedReader buffer2 = new BufferedReader(fileReader2);
//			 
//			 
//			   fileWriter = new FileWriter("average.txt");
//			   PrintWriter out = new PrintWriter(fileWriter);
//			   out.println("Open file:");
//			   out.println("\t---- Homework Score ----\n");
//			   out.println("\t Name \t\t Average score");
//			   String line = buffer.readLine();
//			   while (line != null) {
//			    String[] message = line.split(",");
//			    String name = message[0].trim();
//			    int amount = (message.length-1);
//			    double sum = 0;
//			    for(int i=1;i<message.length;i++){
//			     sum += Double.parseDouble(message[i]);
//			    }
//			    
//			    Score s = new Score(name, sum, amount);
//			    out.println(s.getAverage());   
//			    line = buffer.readLine();
//			   }  
//			   out.println("\n\t---- Exam Score ----\n");
//			   out.println("\t Name \t\t Average score"); 
//			   String line2 = buffer2.readLine();
//			   while (line2 != null) {
//			    String[] message = line2.split(",");
//			    String name = message[0].trim();
//			    int amount = (message.length-1);
//			    double sum = 0;
//			    for(int i=1;i<message.length;i++){
//			     sum += Double.parseDouble(message[i]);
//			    }
//			    
//			    Score s = new Score(name, sum, amount);
//			    out.println(s.getAverage());   
//			    line2 = buffer2.readLine();
//			   }    
//			  
//			  
//			    out.flush();
//			    }
//			  
//			   
//			    
//			    catch (IOException e){
//			    System.err.println("Error reading from file");
//			    }
//			    finally {
//				    
//				    }
//
//	}
//
//}